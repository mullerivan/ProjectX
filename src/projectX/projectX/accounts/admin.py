from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from reversion.admin import VersionAdmin
from projectX.accounts.models import User
# Register your models here.

class UserAdmin(BaseUserAdmin, VersionAdmin):

    """Custom User Admin."""

    list_display = (
        'email', 'is_active')
    list_filter = ('is_superuser',)
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()
    readonly_fields = ('login_attempt',)
    fieldsets = (
        (None, {
            'fields': (
                'email',
                'password',
                'login_attempt'
            ),
        }),

        ('Personal info', {
            'fields': (
                'first_name',
                'last_name',
            ),
        }),

        ('Permissions', {
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions',
                'activation_key',
            ),
        }),

        ('Important dates', {
            'fields': (
                'last_login',
                #'date_joined',
            ),
        }),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'password1',
                'password2',
            ),
        }),
    )

admin.site.register(User, UserAdmin)
