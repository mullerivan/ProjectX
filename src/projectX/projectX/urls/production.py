# -*- coding: utf-8 -*-
from django.conf.urls import url, include


urlpatterns = [
    # Include the base urls.
    url(r'', include('projectX.urls.base')),
]
