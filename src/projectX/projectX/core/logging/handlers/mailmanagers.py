# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import logging

from django.core.mail import mail_managers
from django.utils import six


__all__ = ('MailManagersHandler',)


class MailManagersHandler(logging.Handler):

    """Logging handler to send email to managers."""

    def __init__(self, include_html=False, email_backend=None):
        logging.Handler.__init__(self)
        self.include_html = include_html
        self.email_backend = email_backend

    def emit(self, record):
        """."""
        try:
            subject = 'A log {level} has been received and requires action' \
                .format(level=record.levelname)
            message = 'A log {level} has been received and requires action.' \
                .format(level=record.levelname)

            if hasattr(record, 'is_public'):
                if record.is_public is True:
                    message += '\n\nThis message can be viewed by normal users.'

            if hasattr(record, 'context'):
                message += '\n\nThe log message has the following context:\n\n'

                for context_obj in record.context:
                    message += '{obj}\n'.format(obj=six.text_type(context_obj))

            if hasattr(record, 'comments'):
                message += '\n\nThe log message has the following comments:\n\n'

                for comment in record.comments:
                    message += '{obj}\n'.format(obj=six.text_type(comment))

            message = '\n\n{message}'.format(message=self.format(record))

            mail_managers(subject, message, fail_silently=True)
        except (KeyboardInterrupt, SystemExit):
            raise
        except:
            self.handleError(record)
