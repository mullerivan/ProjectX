# -*- coding: utf-8 -*-
# vim: set expandtab tabstop=4 shiftwidth=4:

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from django.dispatch import receiver
from django.db.models import signals
from django.contrib.contenttypes.models import ContentType

from projectX.core import logging
from projectX.core.models import Comment
from projectX.core.models import CommentContext


@receiver(signals.post_save, sender=Comment)
def comment_created(sender, instance, **kwargs):
    """Log every time a comment is created.
    
    Record a log message every time a comment is created because we want all 
    comments to appear in the safekiwi log in case they need action.
    
    """
    if kwargs['created'] is True:
        msg = 'A comment was just created by {by}'.format(by=instance.by)
        logging.info(msg, context=[instance])
    else:
        msg = 'A comment was just edited by {by}'.format(by=instance.by)
        logging.info(msg, context=[instance])


@receiver(signals.post_delete)
def object_deleted(sender, instance, **kwargs):
    """When an object is deleted also delete the related comments if any.
    
    First we get a list of all comments related to this object. Because a 
    comment can have more than a single context we must only delete the 
    comment if the comment has no remaining context.

    Also because a comment may have children, we must also delete a comments 
    children.
    
    """
    from django.contrib.sessions.models import Session
    if not isinstance(instance, Session):
        comments = Comment.objects.for_(instance)
        for comment in comments:
            ct = ContentType.objects.get_for_model(instance)
            qs = CommentContext.objects \
                .filter(comment=comment) \
                .exclude(content_type=ct, object_id=instance.pk)
            if qs.count() == 0:
                for child in comment.get_children():
                    child.delete()
                comment.delete()
