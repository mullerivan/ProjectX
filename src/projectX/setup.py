import os
from setuptools import setup
from setuptools import find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(os.path.abspath(__file__)), fname)).read()


setup(
    name='projectX',
    version='0.1',
    author='Ivan',
    author_email='mullerivan@gmail.com',
    url='http://mullerivan.com',
    description="None",
    long_description=read('README'),
    license=read('LICENSE'),
    packages=find_packages(),
    zip_safe=False,
    install_requires=[
        'setuptools',
    ]
)
